package cz.cvut.fel.ts1;

public class Calculator {

    public int add(int a, int b)
            {
        return a + b;
    }
    public int subtract(int a, int b) {
        return a-b;
    }
    public int mulitply(int a, int b) {
        return a * b;
    }
    public int divide(int a, int b) throws IllegalArgumentException {
        if (b == 0) {
            throw new IllegalArgumentException("dělení 0!");
        }
        return a/b;
    }

    public void throwException() throws Exception {
        throw new Exception("!!");
    }

}
