package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CalculatorTest {

    Calculator calculator;

    @BeforeEach
    public void setup() {
        calculator = new Calculator();
    }


    @Test
    public void add_5Plus6_returns11() {
        // arrange
        int a = 5;
        int b = 6;
        int expected = 11;

        // act
        int result = calculator.add(a, b);

        // assert
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void subtract_10minus6_returns4() {
        // arrange
        int a = 10;
        int b = 6;
        int expected = 4;

        // act
        int result = calculator.subtract(a, b);

        // assert
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void multiply_5Times6_returns30() {
        // arrange
        int a = 5;
        int b = 6;
        int expected = 30;

        // act
        int result = calculator.mulitply(a, b);

        // assert
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void divide_10Divide2_returns5() {
        // arrange
        int a = 10;
        int b = 2;
        int expected = 5;

        // act
        int result = calculator.divide(a, b);

        // assert
        Assertions.assertEquals(expected, result);
    }

    @Test
    public void divide_10Divide0_throwsException() {

        int a = 10;
        int b = 0;

        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            calculator.divide(a,b);
        });

        String expected = "dělení 0!";
        String actual = exception.getMessage();

        Assertions.assertTrue(actual.contains(expected));
    }


    @Test
    public void throwException_default_exceptionIsThrown() {
        Assertions.assertThrows(Exception.class, () -> {
            calculator.throwException();
        });
    }
}
